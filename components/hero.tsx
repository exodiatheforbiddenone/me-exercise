export interface HeroProps{
    title?: string;
    subTitle?: string;
}

const Hero: React.FC<HeroProps> = ({ title, subTitle }) => (
    <div className="container col-xxl-8 px-4 py-5 pb-0 mb-0 ">
            <div className="row flex-lg-row-reverse align-items-center g-5 py-5">
              <div className="col-10 col-sm-8 col-lg-6">
                <img
                  src="/school.svg"
                  className="d-block mx-lg-auto img-fluid"
                  alt="Bootstrap Themes"
                  loading="lazy"
                />
              </div>
              <div className="col-lg-6">
                <h1 className="display-5 fw-bold lh-1 mb-3">
                  Find the
                  <br /> university thats
                  <br /> right for you.
                </h1>
                <p className="lead">
                  Tenetur ex explicabo et illo. Recusandae fugit eius
                  voluptatem. Voluptas atque autem totam.
                </p>
              </div>
            </div>
          </div>
)

export default Hero;