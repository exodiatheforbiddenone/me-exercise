export interface NavBarProps {
  title: string;
  links?: string[];
}
const NavBar: React.FC<NavBarProps> = ({ title, links }) => (
  <nav className="navbar navbar-expand-lg  navbar-light bg-white ">
    <div className="container-fluid">
      <a className="navbar-brand ms-5 fw-bold">{title}</a>

      <span className="d-flex navbar-nav me-5">
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="d-flex navbar-nav me-5">
            <li className="nav-item">
              <a
                className="nav-link active fw-normal pb-0 pt-0"
                aria-current="page"
                href="#"
              >
                Product
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link text-dark fw-normal pb-0 pt-0" href="#">
                Download
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link text-dark fw-normal pb-0 pt-0" href="#">
                Pricing
              </a>
            </li>
          </ul>
        </div>
      </span>
    </div>
  </nav>
);

export default NavBar;
