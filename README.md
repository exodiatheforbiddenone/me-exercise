# Eric's Take Home Exercise

Live Demo: [https://me-exercise.vercel.app](https://me-exercise.vercel.app)

# Getting Started

Install dependancies

```jsx
yarn
```

Run the development server

```jsx
yarn dev
```

Open [http://localhost:3000](http://localhost:3000/) with your browser to see the result.

# Tech stack

Next JS - great for CMS/Marketing type of applications

Bootstrap - great generalist style system

# General Philosophies

# Style system

🛠️ Utility first styles is the way CSS scales the best, without ridged CSS classes that are hard to modify over time we save ourselves the maintenance headache.

💖 Well documented via Storybook JS, we want to run our style system separate from our application and treat it as a living breathing documented that is actively worked on.

# State management

🔽 data down, actions up. We want to avoid Bidirectional data flows because they lead to more bugs.

✅ Well defined typed models. Typescript has provided a great way to safeguard Javascript and provide us with an easier way to develop and maintain code over time.

# Component architecture

👼 Component → functional components, pure components. This is the lowest level of components.

👶🏾 👶🏻 Component of components → complexer component made of other components, sometimes we need a component that uses multiple components and this is a way to distinguish that.

👯 Page → A component that addresses higher level concerns, auth, layout, locales, a/b testing, feature toggles etc.

# Testing

💾 Integration testing is the most important, I'm a fan of snapshot testing in regards to catching styling regressions (loving Cypress). Unit testing is a needed as well and provides a way to test at the lowest level, I love jest.
